const APIError = require('../lib/APIError');
const httpStatus = require('http-status');

/**
 * Checks if there is an authenticated user in the request payload.
 */
module.exports = function (req, res, next) {

    if (req.user.isAuthenticated) {
        next();
    } else {
        const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
        return next(err);
    }

}