const mongoose = require('mongoose');
const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const chai = require('chai'); // eslint-disable-line import/newline-after-import
const expect = chai.expect;

const app = require('../../index');

chai.config.includeStack = true;

/**
 * root level hooks
 */
after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

describe('## Account APIs', () => {

  let unique = Math.random() * 10000;

  let user = {
    first_name: 'Test',
    last_name: 'User',
    email: 'test' + unique + '@user.com',
    picture: 'MULTIPART FILE HERE',
    mobile_cc: '39',
    mobile_number: '321786821',
    gender: 'M',
    dob: '2017-11-08'
  };

  describe('# POST /api/account', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/account')
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.status).to.equal('success');

          let resUser = res.body.data;
          expect(resUser.first_name).to.equal(user.first_name);
          expect(resUser.last_name).to.equal(user.last_name);
          // ... should check everything
          user = res.body;
          done();
        })
        .catch(done);
    });
  });

});